<?php
// подключаем пролог
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// подключаем модуль инфоблоки
if (CModule::IncludeModule("iblock")) {
    $CIBlockElement = new CIBlockElement();

    $arFolder = 'my-query-url';

    $arOrder = array(
        'PROPERTY_URL_PAGE' => 'ASC',
    );

    $arFilter = array(
        'IBLOCK_ID' => 100,
        'ACTIVE' => 'Y',
        'PROPERTY_URL_PAGE' => $arFolder . '%',
        '!=CODE' => 'page',
    );

    $arSelect = array(
        'ID',
        'NAME',
        'CODE',
        'SORT',
        'PROPERTY_URL_PAGE',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'PROPERTY_URL_BANNER',
        'PROPERTY_SECTION_NAME',
        'PROPERTY_CONSTRUCTOR_NAME',
        'PROPERTY_ID_BRAND',
        'PROPERTY_SEO_TITLE',
        'PROPERTY_SEO_H1',
        'PROPERTY_SEO_DESCRIPTION'
    );

    $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);

    while ($obElement = $res->GetNextElement()) {
        $arFields = $obElement->GetFields();
        if ($arFields["CODE"] === "page-tab") {
            $arIns[$arFields["CODE"]][$arFields["ID"]] = $arFields;
        } else {
            $arIns[] = $arFields;
        }
    }
}