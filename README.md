# Задание №1

## Что и как можно сделать лучше в коде в рамках поставленной задачи?

### Условие:
Решение задачи должно быть максимально оптимизированным по быстродействию и выполнять все описанные условия в рамках поставленной задачи, код должен быть самодокументируемым.

Есть инфоблок с ID = 100 и символьным кодом CATALOG_SECTION_DATA.

У всех элементов инфоблока CATALOG_SECTION_DATA наполняются следующие поля: NAME, CODE, PREVIEW_TEXT, DETAIL_TEXT, PREVIEW_PICTURE, DETAIL_PICTURE

У инфоблока CATALOG_SECTION_DATA есть свойства:
URL_PAGE, URL_BANNER, SECTION_NAME, CONSTRUCTOR_NAME, ID_BRAND, SEO_TITLE, SEO_H1, SEO_DESCRIPTION

У всех элементов инфоблока CATALOG_SECTION_DATA наполняются следующие свойства: URL_PAGE, URL_BANNER, SECTION_NAME, CONSTRUCTOR_NAME, ID_BRAND, SEO_TITLE, SEO_H1, SEO_DESCRIPTION

### Задача:
Если у нас есть значение в $arFolder, необходимо получить все наполняемые данные всех активных элементов инфоблока CATALOG_SECTION_DATA, у которых значение свойства URL_PAGE начинается со значения $arFolder или равно значению $arFolder и значение поля CODE элемента инфоблока CATALOG_SECTION_DATA не равно page.
Выборка элементов должна быть отсортирована по алфавитному порядку значений свойства URL_PAGE элемента инфоблока CATALOG_SECTION_DATA.
Необходимо собрать результирующий массив следующего вида: если у полученного элемента инфоблока CATALOG_SECTION_DATA поле CODE равно page-tab, тогда записываемый текущий элемент инфоблока в результирующем массиве должен иметь структуру вида:
['значение поля CODE элемента инфоблока']['значение поля ID элемента инфоблока'] => [массив со всеми наполняемыми данными текущего элемента инфоблока CATALOG_SECTION_DATA]

Если у полученного элемента инфоблока CATALOG_SECTION_DATA поле CODE не равно page-tab, тогда записываемый текущий элемент инфоблока в результирующем массиве должен иметь структуру вида:
[автоинкримент] => [массив со всеми наполняемыми данными текущего элемента инфоблока CATALOG_SECTION_DATA]

Реализация задачи должна быть не на D7.


````phpt
$CIBlockElement = new CIBlockElement();

$rs = CIBlockElement::GetList(
    Array(
        'PROPERTY_URL_PAGE' => 'DESC',
        'SORT' => 'ASC',
        'NAME' => 'DESC'
    ),
    array(
        'IBLOCK_ID' => 100,
        'PROPERTY_URL_PAGE' => $arFolder
    ),
    false,
    false,
    array(
        'ID',
        'NAME',
        'CODE',
        'SORT',
        'PROPERTY_URL_PAGE',
        'PREVIEW_TEXT',
        'DETAIL_TEXT',
        'PREVIEW_PICTURE',
        'PROPERTY_URL_BANNER',
        'PROPERTY_SECTION_NAME',
        'PROPERTY_CONSTRUCTOR_NAME',
        'PROPERTY_OLD_TEXT',
        'PROPERTY_ID_BRAND',
        'PROPERTY_SEO_TITLE',
        'PROPERTY_SEO_H1',
        'PROPERTY_SEO_DESCRIPTION'
    )
);

while($ob = $rs->GetNextElement()) {
    $arFields = $ob->GetFields();
    if ($arFields["CODE"] != "page") {
        if ($arFields["CODE"] == "page-tab") {
            $arIns[$arFields["CODE"]][$arFields["ID"]] = $arFields;
        } else {
            $arIns[] = $arFields;
        }
    }
}

```